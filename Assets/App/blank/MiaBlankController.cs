using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class MiaBlankController : MonoBehaviour
{
    private MiaBlankInput miaBlankInput;
    public CharacterController controller;
    private Vector3 playerVelocity;
    public bool groundedPlayer;
    public float maxPlayerSpeed = 2.0f;
    public float jumpHeight = 1.0f;
    public float gravityValue = -9.81f;
    public Transform camTrans;
    public Transform child;
    public float rotSpeed;
    public Animator animator;
    private static readonly int Jump = Animator.StringToHash("jump");
    private static readonly int Speed = Animator.StringToHash("speed");
    private MiaMood miaMood;
    private static readonly int Punch = Animator.StringToHash("punch");
    private static readonly int Play = Animator.StringToHash("play");
    public float playtimeDelay;
    public Transform origPos;
    public Transform origCamPos;
    public CinemachineFreeLook playerCam;
    public CinemachineVirtualCamera overViewCam;
    private bool playerCamOn = false;
    private EnemyController enemy;
    public CinemachineVirtualCameraBase[] cameras;
    public CinemachineVirtualCameraBase currentCamera;
    public enum CameraView
    {
        Overview,
        Player,
        Fight
    }

    private void Awake()
    {
        miaBlankInput = new MiaBlankInput();
    }

    private void OnEnable()
    {
        miaBlankInput.Enable();
    }

    private void OnDisable()
    {
        miaBlankInput.Disable();
    }

    private void Start()
    {
        //camAnimator.Play("Player");
        // transform.position = origPos.position;
        foreach (var cameraBase in cameras)
        {
            cameraBase.enabled = false;
        }
        SwitchCameras(CameraView.Overview);
        ChangeMood(new MoodWalk(this));
        StartCoroutine(waitZoom());
    }

    void SwitchCameras(CameraView camera)
    {
        int camIndex = (int) camera;

        cameras[camIndex].enabled = true;
        if (currentCamera != null)
        {
            currentCamera.enabled = false;
        }
        
        currentCamera = cameras[camIndex];
       /* if (!playerCamOn)
        {
            playerCam.Priority = 1;
            overViewCam.Priority = 0;
        }
        else
        {
            playerCam.Priority = 0;
            overViewCam.Priority = 1;
        }

        playerCamOn = !playerCamOn;*/
    }

    public void ChangeMood(MiaMood mood)
    {
        miaMood = mood;
        miaMood.Start();
    }

    IEnumerator waitZoom()
    {
        yield return new WaitForSeconds(1f);
        SwitchCameras(CameraView.Player);
    }

    void GotoJump()
    {
        animator.SetBool(Jump, true);
    }

    void StopJump()
    {
        animator.SetBool(Jump, false);
    }

    void ApplyAnimSpeed(float speed)
    {
        speed = Mathf.Clamp01(speed);
        animator.SetFloat(Speed, speed);
    }

    void RandomMirror()
    {
        var xScale = Random.value<0.5f ? 1 : -1;
        child.localScale = Vector3.Scale(child.localScale, new Vector3(xScale, 1, 1));
    }

    void GotoFight()
    {
        RandomMirror();
        animator.SetBool(Punch, true);
    }

    void GotoRepeatFight()
    {
        RandomMirror();
        animator.Play("decidepunch", -1, 0.0f);
    }

    void GotoPlay()
    {
        animator.SetBool(Play, true);
    }

    public void FightOver()
    {
        SwitchCameras(CameraView.Player);
        ChangeMood(new MoodWalk(this));
    }

    void ResetAnimatorVariables()
    {
        animator.SetBool(Play, false);
        animator.SetBool(Punch, false);
        ApplyAnimSpeed(0);
        StopJump();
    }

    void Update()
    {
        miaMood.Update();
        if (transform.position.y < -70)
        {
            transform.position = origPos.position;
            ChangeMood(new MoodWalk(this));
            ResetAnimatorVariables();
        }
    }

    public void StartRealFight(EnemyController enemy)
    {
        this.enemy = enemy;
        ChangeMood(new MoodRealFight(this));
    }

    private void HandleRotation(Vector2 moveInput, Vector3 move)
    {
        if (moveInput != Vector2.zero)
        {
            //Quaternion rot = Quaternion.Euler(new Vector3(child.localEulerAngles.x, camTrans.localEulerAngles.y, child.localEulerAngles.z));
            Quaternion rot = Quaternion.LookRotation(move, child.up);

            child.rotation =
                Quaternion.Lerp(child.rotation, rot, Time.deltaTime * rotSpeed);
        }
    }

    private void HandleJump()
    {
        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }

    private Vector3 HandleMove(Vector2 moveInput)
    {
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

//        Debug.Log(playerVelocity.y);


        Vector3 move = camTrans.forward * moveInput.y + camTrans.right * moveInput.x;
        move.y = 0;

        //new Vector3(moveInput.x, 0, moveInput.y);
        var playerMoveVector = move * Time.deltaTime * maxPlayerSpeed;
        controller.Move(playerMoveVector);
        return move;
    }

    public abstract class MiaMood
    {
        protected MiaMood(MiaBlankController controller)
        {
            this.controller = controller;
        }


        public MiaBlankController controller;

        public abstract void Start();
        public abstract void Update();
    }

    public class MoodWalk : MiaMood
    {
        private float startTime;


        public MoodWalk(MiaBlankController controller) : base(controller)
        {
        }

        public override void Start()
        {
            startTime = Time.time;
        }

        public override void Update()
        {
            if (controller.miaBlankInput.Main.fight.triggered)
            {
                controller.ChangeMood(new MoodPracticeFight(controller));
                return;
            }

            if (controller.miaBlankInput.Main.play.triggered)
            {
                controller.ChangeMood(new MoodPlay(controller));
                return;
            }

            Vector2 moveInput = controller.miaBlankInput.Main.move.ReadValue<Vector2>();

            var move = controller.HandleMove(moveInput);

            if (controller.miaBlankInput.Main.jump.triggered && controller.groundedPlayer)
            {
                controller.playerVelocity.y += Mathf.Sqrt(controller.jumpHeight * -3.0f * controller.gravityValue);
                controller.ChangeMood(new MoodJump(controller));
                return;
            }

            controller.HandleJump();

            controller.HandleRotation(moveInput, move);

            controller.ApplyAnimSpeed(moveInput.magnitude);

            if (Time.time - startTime > controller.playtimeDelay)
            {
                controller.ChangeMood(new MoodPlay(controller));
            }
        }
    }

    public class MoodRealFight : MiaMood
    {
        public MoodRealFight(MiaBlankController controller) : base(controller)
        {
        }

        void LookAtEnemy()
        {
            //controller.controller.transform.LookAt(controller.enemy.transform);
            
            var dir = controller.enemy.transform.position-controller.child.transform.position ;
            Quaternion rot = Quaternion.LookRotation(dir, controller.transform.up);
            //controller.child.transform.LookAt(controller.enemy.transform);
            controller.child.transform.rotation = Quaternion.Euler(0, rot.eulerAngles.y,0);
            //controller.controller.transform.rotation = Quaternion.Euler(0,, 0);
        }
        
        public override void Start()
        {
            controller.ResetAnimatorVariables();
            controller.SwitchCameras(CameraView.Fight);
            LookAtEnemy();
        }

        public override void Update()
        {
            LookAtEnemy();
            if (controller.miaBlankInput.Main.fight.triggered)
            {
                controller.GotoRepeatFight();
                controller.enemy.TakeHit();
            }
/*
            if (controller.animator.GetBool(Punch) == false)
            {
                controller.ChangeMood(new MoodWalk(controller));
            }*/
        }
    }

    public class MoodPracticeFight : MiaMood
    {
        public MoodPracticeFight(MiaBlankController controller) : base(controller)
        {
        }

        public override void Start()
        {
            controller.GotoFight();
        }

        public override void Update()
        {
            if (controller.miaBlankInput.Main.fight.triggered)
            {
                controller.GotoRepeatFight();
            }

            if (controller.animator.GetBool(Punch) == false)
            {
                controller.ChangeMood(new MoodWalk(controller));
            }
        }
    }

    public class MoodJump : MiaMood
    {
        public MoodJump(MiaBlankController controller) : base(controller)
        {
        }

        public override void Start()
        {
            controller.GotoJump();
        }

        public override void Update()
        {
            Vector2 moveInput = controller.miaBlankInput.Main.move.ReadValue<Vector2>();

            controller.HandleMove(moveInput);
//            Debug.Log(controller.playerVelocity);
            //!! must be before handle jump
            if (controller.playerVelocity.y == 0)
            {
//                Debug.Log("ground");
                controller.StopJump();
                controller.ChangeMood(new MoodWalk(controller));
                return;
            }

            controller.HandleJump();
        }
    }

    public class MoodPlay : MiaMood
    {
        public MoodPlay(MiaBlankController controller) : base(controller)
        {
        }

        public override void Start()
        {
            controller.GotoPlay();
        }

        public override void Update()
        {
            if (controller.animator.GetBool(Play) == false)
            {
                controller.ChangeMood(new MoodWalk(controller));
            }
        }
    }
}