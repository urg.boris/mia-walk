using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraLook : MonoBehaviour
{
    public CinemachineFreeLook cinemachine;

    public AnimationCurve curve;
    private MiaBlankInput miaBlankInput;
    public float lookSpeed;

    private void Awake()
    {
        miaBlankInput = new MiaBlankInput();
    }
    
    private void OnEnable()
    {
        miaBlankInput.Enable();
    }

    private void OnDisable()
    {
        miaBlankInput.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 delta = miaBlankInput.Main.look.ReadValue<Vector2>()*2;
//        Debug.Log(delta);
        var curved = new Vector2(curve.Evaluate(delta.x), curve.Evaluate(delta.y));
        cinemachine.m_XAxis.Value += curved.x * 50*lookSpeed * Time.deltaTime;
        cinemachine.m_YAxis.Value += curved.y * lookSpeed * Time.deltaTime;
    }
}
