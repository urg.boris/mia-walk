using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndDecide : StateMachineBehaviour
{
    private static readonly int play = Animator.StringToHash("play");
    

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(play, false);

    }


}
