using System.Collections;
using System.Collections.Generic;
//using UnityEditor.VersionControl;
using UnityEngine;

public class TimeToDecide : StateMachineBehaviour
{
    //public int loop = 0;

    public int states;
    //private static readonly int ToDecide = Animator.StringToHash("timeToDecide");
    private static readonly int Brain = Animator.StringToHash("brain");


    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var index = animator.GetInteger(Brain);
        var stateIndex = ((index) % states)+1;
        //Debug.Log(stateIndex);
        animator.SetInteger(Brain, stateIndex);
       // animator.SetBool(ToDecide, true);
    }

    int Decide()
    {
        return Random.Range(0, states)+1;
    }



    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
