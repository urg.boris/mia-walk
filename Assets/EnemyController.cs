using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class EnemyController : MonoBehaviour
{
    public MiaBlankController player;
    public CharacterController CharacterController;
    public float gravityValue = -9.81f;
    private Vector3 velocity;
    public float closeToActiveDist;
    public float closeToAproachDist;
    public float closeToFightDist;
    [FormerlySerializedAs("playerEscapedDist")] public float farBackToIdleDist;
    public float farToInactiveDist;
    
    public float speed;
    public Animator animator;
    private static readonly int Jump = Animator.StringToHash("jump");
    private static readonly int Speed = Animator.StringToHash("speed");
    private static readonly int Punch = Animator.StringToHash("punch");
    private static readonly int Play = Animator.StringToHash("play");
    public ParticleSystem blood;
    public ParticleSystem headBlood;
    public ParticleSystem marker;
    public GameObject childModel;

    private EnemyMood currentMood;
    public int initLifes = 3;
    private int lifes;
    private static readonly int Die1 = Animator.StringToHash("die");
    public Vector2 markerScales;

    // Start is called before the first frame update
    void Start()
    {
        ResetAnimatorVariables();
        SwitchState(new InactiveMood(this));
        lifes = initLifes;
    }
    
    public void SwitchState(EnemyMood newState)
    {
        currentMood = newState;
        currentMood.Start();
    }
/*
    public void SetMarkerBig()
    {
        marker.gameObject.SetActive(true);
        marker.transform.localScale = new Vector3(1, markerScales.y, 1);
    }
    
    public void SetMarkerSmall()
    {
        marker.gameObject.SetActive(true);
        marker.transform.localScale = new Vector3(1, markerScales.x, 1);
    }*/

    public void SetMarkerScale()
    {
        float tDist = Mathf.InverseLerp(closeToAproachDist, 100,GetDistFromPlayer());
        //Debug.Log("tDist: "+tDist);
        float scale = Mathf.Lerp(markerScales.x, markerScales.y, tDist);
        //Debug.Log("scale: "+scale);
        marker.transform.localScale = new Vector3(1, scale, 1);
    }

    public void HideMarker()
    {
        marker.gameObject.SetActive(false);
    }

    public void ShowMarker()
    {
        marker.gameObject.SetActive(true);
    }
    
    public void MakeActive()
    {
        childModel.SetActive(true);
        blood.gameObject.SetActive(true);
        headBlood.gameObject.SetActive(true);
        //ShowMarker();
        //SetMarkerSmall();
    }

    public void MakeInactive()
    {
        childModel.SetActive(false);
        blood.gameObject.SetActive(false);
        headBlood.gameObject.SetActive(false);
        //SetMarkerBig();
    }

    public float GetDistFromPlayer()
    {
        return Vector3.Distance(transform.position, player.transform.position);
    }
        

    // Update is called once per frame
    void Update()
    {
        currentMood?.Update();
    }
    void RandomMirror()
    {
        var xScale = Random.value<0.5f ? 1 : -1;
        transform.localScale = Vector3.Scale(transform.localScale, new Vector3(xScale, 1, 1));
    }

    public void TakeHit()
    {
        headBlood.Play();
        lifes--;
        if (lifes <= 0)
        {
            SwitchState(new MoodDie(this));
        }
    }

    void Die()
    {
        Debug.Log("diee");
        animator.SetBool(Die1, true);
        blood.Play();
        player.FightOver();
    }
    void ApplyAnimSpeed(float speed)
    {
        speed = Mathf.Clamp01(speed);
        animator.SetFloat(Speed, speed);
    }

    void GotoFight()
    {
        RandomMirror();
        animator.SetBool(Punch, true);
    }

    void GotoPlay()
    {
        animator.SetBool(Play, true);
    }

    void ResetAnimatorVariables()
    {
        animator.SetBool(Play, false);
        animator.SetBool(Punch, false);
        ApplyAnimSpeed(0);
    }

    public abstract class EnemyMood
    {
        protected EnemyMood(EnemyController controller)
        {
            this.controller = controller;
        }


        public EnemyController controller;

        public abstract void Start();
        public abstract void Update();
    }
    
    public class InactiveMood : EnemyMood
    {
        public InactiveMood(EnemyController controller) : base(controller)
        {
        }

        public override void Start()
        {
            controller.MakeInactive();
            controller.ShowMarker();
        }

        public override void Update()
        {
            controller.SetMarkerScale(); 
            if(controller.GetDistFromPlayer() < controller.closeToActiveDist)
            {
                controller.MakeActive();
                controller.SwitchState(new MoodIdle(controller));
            }
            controller.velocity.y += controller.gravityValue * Time.deltaTime;
            controller.CharacterController.Move(controller.velocity * Time.deltaTime);
        }
    }

    public class MoodIdle : EnemyMood
    {
        public MoodIdle(EnemyController controller) : base(controller)
        {
        }

        public override void Start()
        {
            controller.velocity = Vector3.zero;
            controller.ResetAnimatorVariables();
        }

        public override void Update()
        {
            controller.SetMarkerScale();            
            controller.velocity.y += controller.gravityValue * Time.deltaTime;
            controller.CharacterController.Move(controller.velocity * Time.deltaTime);
            
            if (controller.GetDistFromPlayer() < controller.closeToAproachDist)
            {
//                Debug.Log("gotoapproach");
                controller.SwitchState(new MoodApproach(controller));
                return;
            }
            
            if(controller.GetDistFromPlayer() < controller.closeToActiveDist)
            {
                controller.MakeActive();
                return;
            }

            if(controller.GetDistFromPlayer() > controller.farToInactiveDist)
            {
                controller.SwitchState(new InactiveMood(controller));
                return;
            }
        }
    }

    public class MoodApproach : EnemyMood
    {
        

        public MoodApproach(EnemyController controller) : base(controller)
        {
        }

        public override void Start()
        {
            controller.HideMarker();
        }

        public override void Update()
        {
            //Debug.Log(Vector3.Distance(controller.transform.position, controller.player.transform.position));
            if (controller.GetDistFromPlayer() < controller.closeToFightDist)
            {
                //Debug.Log("gotofight");
                controller.SwitchState(new MoodFight(controller));
                return;
            }
            
            if (controller.GetDistFromPlayer() > controller.farBackToIdleDist)
            {
                //Debug.Log("gotoidle");
                controller.SwitchState(new MoodIdle(controller));
                
                return;
            }
            controller.ApplyAnimSpeed(0.3f);
            var transform1 = controller.player.transform;
            Vector3 direction = transform1.position - controller.transform.position;
            controller.velocity = direction.normalized * 2;
            controller.velocity.y += controller.gravityValue * Time.deltaTime;
            controller.CharacterController.transform.LookAt(transform1);
            controller.CharacterController.Move(controller.velocity * Time.deltaTime * controller.speed);
            
        }
    }


public class MoodFight : EnemyMood
{
    public MoodFight(EnemyController controller) : base(controller)
    {
    }

    public override void Start()
    {
        controller.velocity = Vector3.zero;
        controller.ResetAnimatorVariables();
        controller.GotoFight();
        controller.player.StartRealFight(controller);
        
    }

    public override void Update()
    {
        

    }
}

public class MoodDie : EnemyMood
{
    public MoodDie(EnemyController controller) : base(controller)
    {
    }

    public override void Start()
    {
        controller.Die();
        
    }

    public override void Update()
    {
        if (controller.GetDistFromPlayer() > controller.farToInactiveDist)
        {
            Destroy(controller.gameObject);
        }
    }
}
}


